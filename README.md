# Usage Instructions
    This simple script sends and post API call to ClouDNS to order a new domain names. It can process a list of domain names with their respective name servers. This script only uses Required fields of the API call.
## Python usage
    Install dependencies
    ```
        python -m pip install -r requirements.txt
    ```

    To invoke the script with default settings run the following command
    ```
        python ./order_new_domain.py
    ```
    Script can be invoked with custom argument
    ```
        python ./order_new_domain.py ./config/your_config.csv ./data/your_domainnames.csv
    ```

    Script depends on libraries:
        pandas
        requests

## CSV files table format
    The script will look for files in ./data/hostname.csv and ./config/config.csv by default.
    Where config.csv contains parameters used in the API call and hostname.csv contain domain names with their respective name servers.
    The default files inside the folders ./data and ./config are examples of the table structure. They were made with LibreOffice Calc for reference.
    Config.csv is 2 rows with every column containing parameter name then the value on the 1st and 2nd rows respectively.
    Hostname.csv is 2 columns with 1st being domain name and 2nd is 0, 1 or many name servers. Array of name servers are separated by comma and enclosed in double quotes.
