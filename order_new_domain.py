import requests
import pandas as pd

#TODO use logger lib (optional)
#TODO CMake analog for python for binary installation (pyinstaller)
#TODO README.md with build and start instrucitons. Write how to use pyinstaller

base_url = "https://api.cloudns.net/domains/order-new-domain.json"

def main (hostnames = "./data/hostname.csv", config = "./config/config.csv") -> int: 

	#Loading tlds into memory
	tlds = "./tlds.txt"
	tlds_file = open(tlds, "r")
	tlds_file_lines = tlds_file.readlines()
	tlds_file.close()

	#loading hostnames and config into memory
	hostnames_list = pd.read_csv(hostnames).to_numpy()
	config = pd.read_csv(config).to_dict("list")

	print("Starting domain name order")

	for domain_name_and_ns in hostnames_list:

		domain_name, tld = domain_name_and_ns[0].split(".", 1)

		if not (tld + "\n" in tlds_file_lines):
			print(f"Invalid tld: {tld} in {domain_name_and_ns[0]}")
			continue

		params = (f'auth-id={config["auth_id"][0]}&auth-password={config["auth_password"][0]}'
				  f'&domain-name={domain_name}&tld={tld}&period={config["period"][0]}'
				  f'&mail={config["mail"][0]}&name={config["name"][0]}'
				  f'&company={config["company"][0]}&address={config["address"][0]}'
				  f'&city={config["city"][0]}&state={config["state"][0]}&zip={config["zip_code"][0]}'
				  f'&country={config["country"][0]}&telno={config["telno"][0]}&telnocc={config["telnocc"][0]}')

		if not type(domain_name_and_ns[1]) is float:
			ns_array = domain_name_and_ns[1].split(",")
			for ns in ns_array:
				params += '&ns[]=' + ns

		result = requests.post(url = base_url, params = params)
		print(f"Response for name: {domain_name} is: {result.text}")

	return 0

main()
